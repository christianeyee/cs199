from pox.core import core
import pox
log = core.getLogger()

import pox.lib.packet as pkt
from pox.lib.packet.ethernet import ethernet, ETHER_BROADCAST, ETHER_ANY
from pox.lib.packet.tcp import tcp
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.dhcp import dhcp
from pox.lib.packet.arp import arp
from pox.lib.packet.icmp import icmp
from pox.lib.addresses import IPAddr, EthAddr,	IP_ANY, IP_BROADCAST
from pox.lib.util import str_to_bool, dpidToStr
from pox.lib.recoco import Timer
from pox.lib.revent import *
import pox.openflow.libopenflow_01 as of

from netaddr import *
import time

NETWORK = IPNetwork('10.0.0.0/24')
IP_DHCP = IPAddr('10.0.0.251')
IP_ATTACKER = IPAddr('10.0.0.252')
IP_HONEYD = IPAddr('10.0.0.253')

# Timeout for flow rules
FLOW_HARD_TIMEOUT = 10 # libopenflow pack error - max value is unsigned short

# Timeout for unmatched flows
FLOW_IDLE_TIMEOUT = 86400/2

# Timeout for ARP entries
ARP_TIMEOUT = 10

# Wait time for ARP reply
ARP_WAIT = 10

# Maximum number of packet to buffer on a switch for an unknown IP
MAX_BUFFERED_PER_IP = 5

# Maximum time to hang on to a buffer for an unknown IP in seconds
MAX_BUFFER_TIME = 5

class Entry (object):
	"""
	Not strictly an ARP entry.
	We use the port to determine which port to forward traffic out of.
	We use the MAC to answer ARP replies.
	We use the timeout so that if an entry is older than ARP_TIMEOUT, we
	flood the ARP request rather than try to answer it ourselves.
	"""
	def __init__ (self, port, mac):
		self.timeout = time.time() + ARP_TIMEOUT
		self.port = port
		self.mac = mac

	def __eq__ (self, other):
		if type(other) == tuple:
			return (self.port,self.mac)==other
		else:
			return (self.port,self.mac)==(other.port,other.mac)
	def __ne__ (self, other):
		return not self.__eq__(other)
	
	def isExpired (self):
		if self.port == of.OFPP_NONE: return False
		return time.time() > self.timeout
	

def dpid_to_mac (dpid):
	return EthAddr("%012x" % (dpid & 0xffFFffFFffFF,))


class l3_switch (EventMixin):
	
	DHCP_TYPES = {'discover': "MsgType(DISCOVER)",
				'offer': "MsgType(OFFER)",
				'request': "MsgType(REQUEST)",
				'ack': "MsgType(ACK)",
				'nak': "MsgType(NAK)",
				'release': "MsgType(RELEASE)",
				'inform': "MsgType(INFORM)",
				'decline': "MsgType(DECLINE)"
				}
		
	
	def __init__ (self, fakeways = [], arp_for_unknowns = False):
		# These are "fake gateways" -- we'll answer ARPs for them with MAC
		# of the switch they're connected to.
		self.fakeways = set(fakeways)

		# If this is true and we see a packet for an unknown
		# host, we'll ARP for it.
		self.arp_for_unknowns = arp_for_unknowns

		# (dpid,IP) -> expire_time
		# We use this to keep from spamming ARPs
		self.outstanding_arps = {}
		
		# (IP) -> expire_time
		# We use this to track long pending ARPs
		self.unanswered_arps = {}

		# (dpid,IP) -> [(expire_time,buffer_id,in_port), ...]
		# These are buffers we've gotten at this datapath for this IP which
		# we can't deliver because we don't know where they go.
		self.lost_buffers = {}

		# For each switch, we map IP addresses to Entries
		self.arpTable = {}

		# This timer handles expiring stuff
		self._expire_timer = Timer(5, self._handle_expiration, recurring=True)
		
		self.used_ips = set()
		self.used_ips.add(IP_DHCP)
		self.used_ips.add(IP_ATTACKER)
		self.used_ips.add(IP_HONEYD)
		
		self.honeyd_dpid = -1

		self.listenTo(core)
		

	def _handle_GoingUpEvent (self, event):
	 self.listenTo(core.openflow)
		

	def _handle_expiration (self):
		# Called by a timer so that we can remove old items.
		
		# Assume ip unoccupied for long unanswered ARP
		for ip in self.unanswered_arps.keys():
			started = self.unanswered_arps[ip]
			if (time.time() - started) > ARP_WAIT:
				if ip in self.used_ips:
					self.used_ips.remove(ip)
					self._delete_arp_entries(ip)
					self._delete_flows(ip)
				del self.unanswered_arps[ip]
		
		
		empty = []
		for k,v in self.lost_buffers.iteritems():
			dpid,ip = k

			for item in list(v):
				expires_at,buffer_id,in_port = item
				if expires_at < time.time():
					# This packet is old.	Tell this switch to drop it.
					v.remove(item)
					po = of.ofp_packet_out(buffer_id = buffer_id, in_port = in_port)
					core.openflow.sendToDPID(dpid, po)
			if len(v) == 0: empty.append(k)

		# Remove empty buffer bins
		for k in empty:
			del self.lost_buffers[k]


	def _send_lost_buffers (self, dpid, ipaddr, mac, port):
		"""
		We may have "lost" buffers -- packets we got but didn't know
		where to send at the time.	We may know now.	Try and see.
		"""
		if (dpid,ipaddr) in self.lost_buffers:
			# Yup!
			bucket = self.lost_buffers[(dpid,ipaddr)]
			del self.lost_buffers[(dpid,ipaddr)]
			log.debug("Sending {0} buffered packets to {1} from {2}"
								.format(len(bucket),ipaddr,dpidToStr(dpid)))
			for _,buffer_id,in_port in bucket:
				msg = of.ofp_packet_out(buffer_id=buffer_id,in_port=in_port)
				msg.actions.append(of.ofp_action_dl_addr.set_dst(macaddr))
				msg.actions.append(of.ofp_action_output(port = port))
				core.openflow.sendToDPID(dpid, msg)

				
	def _flood(self, event):
		msg = of.ofp_packet_out(in_port = event.port, action = of.ofp_action_output(port = of.OFPP_FLOOD))
		if event.ofp.buffer_id is of.NO_BUFFER:
			msg.data = event.data
		else:
			msg.buffer_id = event.ofp.buffer_id
		event.connection.send(msg.pack())
	
	
	def _is_valid_address(self, ip):
		ip = IPAddress(str(ip))

		if ip.is_multicast(): return False 
		if ip.is_loopback(): return False
		if ip.is_reserved(): return False
		if ip.is_unicast(): return True
		if ip.is_private(): return True 
		
		return True

	
	def _is_to_honeyd(self, dst, src=None):
		if not self._is_valid_address(dst): return False

		if src == IP_DHCP: return False
		if src == IP_ANY: return False
		#if src == IP_HONEYD: return False

		if IPAddress(str(dst)) == NETWORK.broadcast: return False 
		if dst == IP_BROADCAST: return False
		if dst == IP_ANY: return False
		if dst in self.used_ips: return False
		
		return True
		

	def _not_from_dhcp(self, src):
		return src != IP_DHCP 

	
	def _arp_for(self, event, packet, dstaddr, dpid, inport):
		# ARP Request 
		r = arp()
		r.hwtype = r.HW_TYPE_ETHERNET
		r.prototype = r.PROTO_TYPE_IP
		r.hwlen = 6
		r.protolen = r.protolen
		r.opcode = r.REQUEST
		r.hwdst = ETHER_BROADCAST
		r.protodst = dstaddr
		r.hwsrc = packet.src
		r.protosrc = packet.next.srcip
		e = ethernet(type=ethernet.ARP_TYPE, src=packet.src, dst=ETHER_BROADCAST)
		e.set_payload(r)
		
		log.debug("{0} {1} ARPing for {2} on behalf of {3}".format(dpid, inport, str(r.protodst), str(r.protosrc)))
	
		msg = of.ofp_packet_out()
		msg.data = e.pack()
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg.in_port = inport
		event.connection.send(msg)
		

	def _arp_for_honeyd(self, event):
		log.debug("ARPing for {0} on behalf of {1}".format(IP_HONEYD, IP_ANY))
		# Gratitious ARP Request
		r = arp()
		r.hwtype = r.HW_TYPE_ETHERNET
		r.prototype = r.PROTO_TYPE_IP
		r.hwlen = 6
		r.protolen = r.protolen
		r.opcode = r.REQUEST
		r.hwdst = ETHER_BROADCAST
		r.protodst = IP_HONEYD
		r.protosrc = 0
		e = ethernet(type=ethernet.ARP_TYPE, dst=ETHER_BROADCAST)
		e.set_payload(r)
	
		msg = of.ofp_packet_out()
		msg.data = e.pack()
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg.in_port = of.OFPP_NONE
		event.connection.send(msg)
		

	def _arp_to(self, event, packet_type, a, dpid, inport):		
		# ARP Reply
		
		if a.protodst in self.unanswered_arps: del self.unanswered_arps[a.protodst]
		
		r = arp()
		r.hwtype = a.hwtype
		r.prototype = a.prototype
		r.hwlen = a.hwlen
		r.protolen = a.protolen
		r.opcode = arp.REPLY
		r.hwdst = a.hwsrc
		r.protodst = a.protosrc
		r.protosrc = a.protodst
		r.hwsrc = self.arpTable[dpid][a.protodst].mac
		e = ethernet(type=packet_type, src=dpid_to_mac(dpid), dst=a.hwsrc)
		e.set_payload(r)
									
		log.debug("{0} {1} answering ARP for {2}".format(dpid, inport, str(r.protosrc)))
									
		msg = of.ofp_packet_out()
		msg.data = e.pack()
		msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
		msg.in_port = inport
		event.connection.send(msg)
		

	def _arp_to_with_honeyd(self, event, packet_type, a, dpid, inport):
		# ARP Reply
		
		if a.protodst in self.unanswered_arps: del self.unanswered_arps[a.protodst]
		
		r = arp()
		r.hwtype = a.hwtype
		r.prototype = a.prototype
		r.hwlen = a.hwlen
		r.protolen = a.protolen
		r.opcode = arp.REPLY
		r.hwdst = a.hwsrc
		r.protodst = a.protosrc
		r.protosrc = a.protodst
		
		try:
			r.hwsrc = self.arpTable[self.honeyd_dpid][IP_HONEYD].mac
			e = ethernet(type=packet_type, src=dpid_to_mac(dpid), dst=a.hwsrc)
			e.set_payload(r)
		
			log.debug("{0} {1} answering ARP for {2} with Honeyd".format(dpid, inport, str(r.protosrc)))
									
			msg = of.ofp_packet_out()
			msg.data = e.pack()
			msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
			msg.in_port = inport
			event.connection.send(msg)
		
		except KeyError:
			self._arp_for_honeyd(event)
			return
			
		
	def _learn_or_update(self, packet, srcip, dpid, inport):
		if srcip == IP_HONEYD:	
			self.honeyd_dpid = dpid

		if srcip in self.arpTable[dpid]:
			if self.arpTable[dpid][srcip] != (inport, packet.src):
				log.info("%i %i RE-learned %s", dpid,inport,srcip)
		else:
			log.debug("%i %i learned %s", dpid,inport,str(srcip))
		self.arpTable[dpid][srcip] = Entry(inport, packet.src)
	
	
	def _install_flow_to_honeyd(self, event, packet, inport):	
		try:		
			port = self.arpTable[self.honeyd_dpid][IP_HONEYD].port
			mac = self.arpTable[self.honeyd_dpid][IP_HONEYD].mac
			
			actions = []
			actions.append(of.ofp_action_dl_addr.set_dst(mac))
			actions.append(of.ofp_action_output(port = port))
			match = of.ofp_match.from_packet(packet, inport)
			
			msg = of.ofp_flow_mod(command=of.OFPFC_MODIFY,
								  idle_timeout=FLOW_IDLE_TIMEOUT,
								  hard_timeout=FLOW_HARD_TIMEOUT,
								  buffer_id=event.ofp.buffer_id,
								  actions=actions,
								  match=match)
			event.connection.send(msg.pack())
			
		except KeyError:
			self._arp_for_honeyd(event)
			return

			
	def _install_flow(self, event, packet, dpid, inport, mac, port):
			
		log.debug("{0} {1} installing flow for {2} => {3} out port {4}"
					.format(dpid, inport, packet.next.srcip, packet.next.dstip, port))

		actions = []
		actions.append(of.ofp_action_dl_addr.set_dst(mac))
		actions.append(of.ofp_action_output(port = port))
		#match = of.ofp_match.from_packet(packet, inport)
		
		match = of.ofp_match()
		match.nw_src = packet.next.srcip
		match.nw_dst = packet.next.dstip
		match.dl_src = packet.src
		match.dl_dst = packet.dst
		match.in_port = inport

		msg = of.ofp_flow_mod(command=of.OFPFC_MODIFY,
							  idle_timeout=FLOW_IDLE_TIMEOUT,
							  hard_timeout=FLOW_HARD_TIMEOUT,
							  buffer_id=event.ofp.buffer_id,
							  actions=actions,
							  match=match)
		event.connection.send(msg.pack())
	
	
	def _delete_flows(self, ip):
		
		ofdst = of.ofp_flow_mod(command=of.OFPFC_DELETE)	
		ofdst.match.nw_dst = ip
		
		ofsrc = of.ofp_flow_mod(command=of.OFPFC_DELETE)
		ofsrc.match.nw_src = ip
		
		for connection in core.openflow.connections:	
			connection.send(ofdst)
			connection.send(ofsrc)
			log.debug("Clearing flows in switch {0} for IP {1}"
						.format(dpidToStr(connection.dpid), str(ip)))
						
						
	def _delete_arp_entries(self, ip):
		for connection in core.openflow.connections:
			if connection.dpid in self.arpTable:
				if ip in self.arpTable[connection.dpid]:
					del self.arpTable[connection.dpid][ip]


	def _is_dhcp(self, event, ip_packet):
		udp_packet = ip_packet.payload
		dhcp_packet = udp_packet.payload
				
		if isinstance(dhcp_packet, dhcp):
			options = dhcp_packet.options
					
			msg_type = repr(options[53])
			'''
			if msg_type == self.DHCP_TYPES['discover']:
				print "DISCOVER"
				print "PREFERRED ADDRESS: " + repr(options.get(50, "None"))
			elif msg_type == self.DHCP_TYPES['offer']:
				print "OFFER"
				print "ADDRESS BEING OFFERED: " + str(dhcp_packet.yiaddr) 
			elif msg_type == self.DHCP_TYPES['request']:
				print "REQUEST"				 
				print "ADDRESS BEING REQUESTED: " + repr(options.get(50, "None"))
			'''
			if msg_type == self.DHCP_TYPES['ack']:
				#print "ACKNOWLEDGE"
				#print "NEWLY-LEASED ADDRESS: " + str(dhcp_packet.yiaddr)
				self.used_ips.add(dhcp_packet.yiaddr)
				self._delete_arp_entries(dhcp_packet.yiaddr)
				self._delete_flows(dhcp_packet.yiaddr)
			elif msg_type == self.DHCP_TYPES['release']:
				#print "RELEASE ADDRESS: " + str(dhcp_packet.ciaddr)
				if dhcp_packet.ciaddr in self.used_ips: 
					self.used_ips.remove(dhcp_packet.ciaddr)
				self._delete_arp_entries(dhcp_packet.ciaddr)
				self._delete_flows(dhcp_packet.ciaddr)
				
			return True

		return False
		

	def _handle_PacketIn (self, event):
	 
		dpid = event.connection.dpid
		inport = event.port
		packet = event.parsed
		
		if not packet.parsed:
			log.warning("%i %i ignoring unparsed packet", dpid, inport)
			return

		if dpid not in self.arpTable:
			# New switch -- create an empty table
			self.arpTable[dpid] = {}
			for fake in self.fakeways:
				self.arpTable[dpid][IPAddr(fake)] = Entry(of.OFPP_NONE, dpid_to_mac(dpid))

		if packet.type == ethernet.LLDP_TYPE:
			# Ignore LLDP packets
			return
		
		if packet.type == ethernet.IP_TYPE:
			ip_packet = packet.payload 

			# Handle DHCP
			if ip_packet.protocol == ipv4.UDP_PROTOCOL:
				if self._is_dhcp(event, ip_packet):
					self._flood(event)
					return 
				

		if isinstance(packet.next, ipv4):
			log.debug("%i %i IP %s => %s", dpid,inport,
								packet.next.srcip,packet.next.dstip)
			
			# Send any waiting packets...
			self._send_lost_buffers(dpid, packet.next.srcip, packet.src, inport)

			# Learn or update port/MAC info
			self._learn_or_update(packet, packet.next.srcip, dpid, inport)
			
			dstaddr = packet.next.dstip

			# Redirect to Honeyd
			if self._is_to_honeyd(dstaddr): 
				self._install_flow_to_honeyd(event, packet, inport)
				return
	 
			if dstaddr in self.arpTable[dpid] and not self.arpTable[dpid][dstaddr].isExpired():
				# We have info about what port to send it out on...

				port = self.arpTable[dpid][dstaddr].port
				mac = self.arpTable[dpid][dstaddr].mac
				
				if port == inport:
					log.warning("{0} {1} not sending packet for {2} back out of the input port"
											.format(dpid, inport, str(dstaddr)))
				else:
					# Install flow
					self._install_flow(event, packet, dpid, inport, mac, port)
					
				return
					
			if self.arp_for_unknowns:		 
				# We don't know this destination.
				# First, we track this buffer so that we can try to resend it later
				# if we learn the destination, second we ARP for the destination,
				# which should ultimately result in it responding and us learning
				# where it is

				# Add to tracked buffers
				if (dpid,dstaddr) not in self.lost_buffers:
					self.lost_buffers[(dpid,dstaddr)] = []
				bucket = self.lost_buffers[(dpid,dstaddr)]
				entry = (time.time() + MAX_BUFFER_TIME, event.ofp.buffer_id, inport)
				bucket.append(entry)
				while len(bucket) > MAX_BUFFERED_PER_IP: del bucket[0]

				# Expire things from our outstanding ARP list...
				self.outstanding_arps = {k:v for k,v in
				 self.outstanding_arps.iteritems() if v > time.time()}

				# Check if we've already ARPed recently
				if (dpid,dstaddr) in self.outstanding_arps:
					# Oop, we've already done this one recently.
					return

				
				self._arp_for(event, packet, dstaddr, dpid, inport)

				return
				

		elif isinstance(packet.next, arp):
			a = packet.next
				
			log.debug("%i %i ARP %s %s => %s", dpid, inport,
			 {arp.REQUEST:"request",arp.REPLY:"reply"}.get(a.opcode,
			 'op:%i' % (a.opcode,)), str(a.protosrc), str(a.protodst))
			 
			if a.prototype == arp.PROTO_TYPE_IP:
				if a.hwtype == arp.HW_TYPE_ETHERNET:
					if a.protosrc != 0:			

						# Learn or update port/MAC info
						self._learn_or_update(packet, a.protosrc, dpid, inport)

						# Send any waiting packets...
						self._send_lost_buffers(dpid, a.protosrc, packet.src, inport)

						# Try to answer ARP Request
						if a.opcode == arp.REQUEST:
	
							# Initial ARPing for Honeyd
							if self.honeyd_dpid == -1 and a.protodst == IP_HONEYD:
								self._flood(event)
								return

							# ARP poison for unused IPs
							if self._is_to_honeyd(a.protodst, a.protosrc):				
								self._arp_to_with_honeyd(event, packet.type, a, dpid, inport)	
								return

							if a.protodst in self.arpTable[dpid] and self._not_from_dhcp(a.protosrc):
								if not self.arpTable[dpid][a.protodst].isExpired():
									self._arp_to(event, packet.type, a, dpid, inport)
									return
									
							# Keep track of this ARP if we cannot answer it
							if a.protodst not in self.unanswered_arps and self._not_from_dhcp(a.protosrc): 
								self.unanswered_arps[a.protodst] = time.time()
					
									
						# ARP Reply
						elif a.opcode == arp.REPLY:
							# Remove from unanswered arps
							if a.protosrc in self.unanswered_arps: del self.unanswered_arps[a.protosrc]


			# Didn't know how to answer or otherwise handle this ARP, so just flood it
			log.debug("%i %i flooding ARP %s %s => %s" % (dpid, inport,
			 {arp.REQUEST:"request",arp.REPLY:"reply"}.get(a.opcode,
			 'op:%i' % (a.opcode,)), str(a.protosrc), str(a.protodst)))

			self._flood(event)



def launch (fakeways="", arp_for_unknowns=None):
	fakeways = fakeways.replace(","," ").split()
	fakeways = [IPAddr(x) for x in fakeways]
	if arp_for_unknowns is None:
		arp_for_unknowns = len(fakeways) > 0
	else:
		arp_for_unknowns = str_to_bool(arp_for_unknowns)
	core.registerNew(l3_switch, fakeways, arp_for_unknowns)


