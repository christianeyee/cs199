#!/usr/bin/python

'''

Experiment for testing controller routing correctness.

'''


from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost, RemoteController
from mininet.log import setLogLevel, info
from mininet.link import TCLink
from mininet.cli import CLI

from time import sleep

import os
import shlex
import sys

from constants import *


class SingleSwitchTopo(Topo):
	"Single switch connected to n hosts."
	def __init__(self, **opts):
		Topo.__init__(self, **opts)
	
		switch = self.addSwitch('s1')
		
		# set up dhcp server
		dhcp = self.addHost( 'dhcp', ip=IP_DHCP )
		self.addLink(dhcp, switch) 

		# set up hosts
		for i in range(MAX_HOSTS):
			host = self.addHost('h%s' % (i+1), ip='0.0.0.0')	
			self.addLink(host, switch)

		# set up attacker		 
		attacker = self.addHost('attacker', ip=IP_ATTACKER)
		self.addLink(attacker, switch)
		
		# set up honeyd
		honeyd = self.addHost('honeyd', ip=IP_HONEYD)
		self.addLink(honeyd, switch)
		

def netBuild(expath):
	topo = SingleSwitchTopo()
	net = Mininet(topo=topo,
				  cleanup = True,
				  #host=CPULimitedHost,
				  #link=TCLink,
				  #autoStaticArp=True,
				  build=False,
				  ipBase=IP_BASE)	
				  	
	net.addController(name='c0',
				   controller=RemoteController,
				   ip='127.0.0.1',
				   protocol='tcp',
				   port=6633)					 
	net.start()
	dhcp = net.get('dhcp')
	attacker = net.get('attacker')
	honeyd = net.get('honeyd')
	startDHCPserver( dhcp, dns='8.8.8.8' )

	# prepare output files and directory
	attacker.cmd('mkdir -p -m 777 %s' % (expath))
	honeyd.cmd('install -b -m 777 /dev/null {fp}/{plog}'.format(fp=expath, plog=P_LOG)) # workaround (honeyd demoted to run as nobody user)
	honeyd.cmd('install -b -m 777 /dev/null {fp}/{slog}'.format(fp=expath, slog=S_LOG)) # service logging	
	
	
	print 'Starting Honeyd with %s%% web servers' % ( 100 )
	honeyd.cmd('sudo honeyd -l {fp}/{plog} -s {fp}/{slog} -p {pdir}/{hdir}/{prints} -f {pdir}/{hdir}/{config}{p}'
				.format(fp=expath, pdir=DIR, hdir=H_DIR, plog=P_LOG, slog=S_LOG, prints=PRINTS, config=CONFIG, p='all'))

	output = open('%s/wget.log' % (os.path.expanduser(expath)), 'w')

	lines = []	
	lines.append('*********Honeyd HTTP Servers***********')
	count = 0 # number of correctly set up honeypot http servers
	print 'Contacting all http servers (honeypots)... '
	for i in range(FROM, TO+1): 
		# verify honeypot http server is up
		ip = '%s%s' % (IP_PREFIX, i)
		response = attacker.cmd('wget --retry-connrefused -t {tries} --waitretry {interval} -O {o} {url}:{port}'
							.format(tries=RESILIENCE, interval=PATIENCE, o='-', url=ip, port=PORT))
		
		if HONEYD_SERVER_RESPONSE in response:
			count += 1
		elif LEGIT_SERVER_RESPONSE in response:
			lines.append('%s\t misdirected to legitimate host' % ( ip )) # impossible
		else:
			lines.append('%s\t responded differently\n %s\n' % ( ip, response ))
		
	lines.append('Correctly routed: {count}/{actual}\n'.format(count=count, actual=TO-FROM+1))
	output.write('\n'.join(lines))
	


	lines = []
	lines.append('*********Legitimate HTTP Servers***********')
	current = 0 # current index, number of honeypots
	count = 0 # number of correctly set up legitimate http servers
	hosts = [] # legitimate hosts
	# bring up INCREMENT hosts at a time until all MAX_HOSTS hosts are up
	while current + INCREMENT <= MAX_HOSTS: 
		print 'Adding %s hosts to %s total...' % ( INCREMENT, current )
		for i in range(current, current+INCREMENT): # next INCREMENT hosts
			host = net.get('h%s' % (i+1))
			# request ip
			mountPrivateResolvconf( host )
			startDHCPclient( host )
			waitForIP( host )

			# run http server
			host.cmd('pushd {pdir}/{mdir}'.format(pdir=DIR, mdir=M_DIR))
			host.cmd('python -m SimpleHTTPServer %s &' % ( PORT ))
			host.cmd('popd')
			
			hosts.append(host)

			# check connectivity, proper redirection
			response = attacker.cmd('wget --retry-connrefused -t {tries} --waitretry {interval} -O {o} {url}:{port}'
							.format(tries=RESILIENCE, interval=PATIENCE, o='-', url=host.IP(), port=PORT))
			
			if LEGIT_SERVER_RESPONSE in response:
				count += 1
			elif HONEYD_SERVER_RESPONSE in response:
				lines.append('%s\t misdirected to Honeyd' % ( host.IP() ))
			else:
				lines.append('%s\t responded differently\n %s\n' % ( host.IP(), response ))

		
		current += INCREMENT

	lines.append('Correctly routed: {count}/{actual}\n'.format(count=count, actual=len(hosts)))
	output.write('\n'.join(lines))



	mid = len(hosts)/2

	lines = []
	count = 0 # number of times ip left hanging
	lines.append('*********Explicit DHCP Release -- 1/2 Hosts***********')
	for i in range(mid):		
		host = hosts[i]
		misses = 0 # requests before honeyd took back ip
		reclaimed = False # honeyd has taken over the ip
		
		host.cmd('dhclient -r') 
		
		while not reclaimed and misses < GRACE:
			# check connectivity, proper redirection
			response = attacker.cmd('wget --retry-connrefused -t {tries} --waitretry {interval} -O {o} {url}:{port}'
							.format(tries=RESILIENCE, interval=PATIENCE, o='-', url=host.IP(), port=PORT))
			
			if HONEYD_SERVER_RESPONSE in response:
				reclaimed = True
			elif LEGIT_SERVER_RESPONSE in response:
				misses += 1
				lines.append('%s\t not yet released' % ( host.IP() ))
			else:
				misses += 1
				lines.append('%s\t responded differently\n %s\n' % ( host.IP(), response ))
				
			if not reclaimed and misses == GRACE: count += 1
	
	lines.append('Unsuccessful honeypot takeovers: {count}/{actual}\n'.format(count=count, actual=len(hosts)/2))
	output.write('\n'.join(lines))


	lines = []
	count = 0
	lines.append('*********Host taken down -- 2/2 Hosts***********')		
	for i in range(mid, len(hosts)):	
		host = hosts[i]
		misses = 0 # requests before honeyd takes back ip
		reclaimed = False # honeyd has taken over the ip
		
		# bring down link to host	
		net.configLinkStatus('s1', '%s' % host, 'down')
		# link dst status change failed: Serving HTTP on 0.0.0.0 port 80 ...
		net.configLinkStatus('s1', '%s' % host, 'down')		
		
		while not reclaimed and misses < GRACE:
			# check connectivity, proper redirection
			response = attacker.cmd('wget --retry-connrefused -t {tries} --waitretry {interval} -O {o} {url}:{port}'
										.format(tries=RESILIENCE, interval=PATIENCE, o='-', url=host.IP(), port=PORT))
			
			if HONEYD_SERVER_RESPONSE in response:
				reclaimed = True
			elif LEGIT_SERVER_RESPONSE in response:
				misses += 1
				lines.append('%s\t not yet released' % ( host.IP() ))
			else:
				misses += 1
				lines.append('%s\t responded differently\n %s\n' % ( host.IP(), response ))
				
			if not reclaimed and misses == GRACE: count += 1 
	
	lines.append('Unsuccessful honeypot takeovers: {count}/{actual}'.format(count=count, actual=len(hosts)/2))
	output.write('\n'.join(lines))

	output.close()
				
	# kill running honeyd daemon
	honeyd.cmd('sudo pkill -SIGINT honeyd')
	
	# cleanup
	stopDHCPserver( dhcp )
	for i in range(MAX_HOSTS):
		host = net.get('h%s' % (i+1))
		stopDHCPclient( host )
		unmountPrivateResolvconf( host )
	net.stop()
	

def checkRequired():
	"Check for required executables"
	required = [ 'udhcpd', 'udhcpc', 'dnsmasq', 'curl', 'firefox' ]
	for r in required:
		if not quietRun( 'which ' + r ):
			print '* Installing', r
			print quietRun( 'apt-get install -y ' + r )
			if r == 'dnsmasq':
				# Don't run dnsmasq by default!
				print quietRun( 'update-rc.d dnsmasq disable' )


# DHCP server functions and data
DNSTemplate = """
start		10.0.0.%s
end		10.0.0.%s
option	subnet	255.255.255.0
option	domain	local
option	lease	86400  # seconds
""" % (FROM, TO) # same address space as honeypots
# option dns 8.8.8.8

def makeDHCPconfig( filename, intf, dns ):
    "Create a DHCP configuration file"
    config = (
        'interface %s' % intf,
        DNSTemplate,
        #'option router %s' % gw,
        'option dns %s' % dns,
        '' )
    with open( filename, 'w' ) as f:
        f.write( '\n'.join( config ) )

def startDHCPserver( host, dns ):
    "Start DHCP server on host with specified DNS server"
    dhcpConfig = '/tmp/%s-udhcpd.conf' % host
    makeDHCPconfig( dhcpConfig, host.defaultIntf(), dns )
    host.cmd( 'udhcpd -f', dhcpConfig,
              '1>/tmp/%s-dhcp.log 2>&1  &' % host )

def stopDHCPserver( host ):
    "Stop DHCP server on host"
    host.cmd( 'kill %udhcpd' )

def startDHCPclient( host ):
    "Start DHCP client on host"
    intf = host.defaultIntf()
    host.cmd( 'dhclient -v -d -r', intf )
    host.cmd( 'dhclient -v -d 1> /tmp/dhclient.log 2>&1', intf, '&' )

def stopDHCPclient( host ):
    host.cmd( 'kill %dhclient' )

def waitForIP( host ):
    "Wait for an IP address"
    while True:
        host.defaultIntf().updateIP()
        if host.IP():
            break
        sleep( 1 )

def mountPrivateResolvconf( host ):
    "Create/mount private /etc/resolv.conf for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'mkdir -p', etc )
    host.cmd( 'mount --bind /etc', etc )
    host.cmd( 'mount -n -t tmpfs tmpfs /etc' )
    host.cmd( 'ln -s %s/* /etc/' % etc )
    host.cmd( 'rm /etc/resolv.conf' )
    host.cmd( 'cp %s/resolv.conf /etc/' % etc )

def unmountPrivateResolvconf( host ):
    "Unmount private /etc dir for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'umount /etc' )
    host.cmd( 'umount', etc )
    host.cmd( 'rmdir', etc )
    
if __name__ == '__main__':
	setLogLevel('info')
	netBuild(100, 
			'{pdir}/{edir}/{folder}/connectivity'
				.format(pdir=DIR, edir=E_DIR, folder=time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime(time.time()))))
