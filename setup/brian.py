#!/usr/bin/python

'''

Mininet setup for testing (with Honeyd)

'''


from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost, RemoteController
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.util import pmonitor
from mininet.examples.nat import stopNAT

from signal import SIGINT
from subprocess import PIPE
from time import sleep

import os
import re
import shlex
import sys
import time
import random

# current directory - repo
DIR = '~/pox'
E_DIR = 'experiments'
H_DIR = 'honeyd'
M_DIR = 'setup'
CONFIG = 'test.conf'
PRINTS = 'nmap-os-db'
P_LOG = 'honeyd.packet.log'
S_LOG = 'honeyd.service.log'

IP_BASE = '10.0.0.0/24'
IP_DHCP = '10.0.0.251'
IP_ATTACKER = '10.0.0.252'
IP_HONEYD = '10.0.0.253'
PORT = 80
MAX_HOSTS = 10
MAX_ACTIVES = 5
TOLERANCE = 3

def execute(attacker, command):

	po = attacker.popen(command, stdout=PIPE, stderr=PIPE, shell=True, bufsize=1)

	count = 0
	keywords = ['service', 'available', 'NO']
	
	for line in iter(po.stdout.readline, b""):
		# terminate after 3rd service down result
		if count >= TOLERANCE:
			po.send_signal( SIGINT )
			return
			
		# look for service down result
		if all(word in line for word in keywords):
			count += 1
			
		yield line


class SingleSwitchTopo(Topo):
	"Single switch connected to n hosts."
	def __init__(self, **opts):
		Topo.__init__(self, **opts)
	
		switch = self.addSwitch('s1')
		
		# set up dhcp server
		dhcp = self.addHost( 'dhcp', ip=IP_DHCP )
		self.addLink(dhcp, switch) 

		# set up hosts
		for i in range(MAX_HOSTS):
			host = self.addHost('h%s' % (i+1))	
			self.addLink(host, switch)

		# set up attacker		 
		attacker = self.addHost('attacker', ip=IP_ATTACKER)
		self.addLink(attacker, switch)
		
		# set up honeyd
		honeyd = self.addHost('honeyd', ip=IP_HONEYD)
		self.addLink(honeyd, switch)
		

def dosTest():
	topo = SingleSwitchTopo()
	net = Mininet(topo=topo,
				  cleanup = True,
				  #host=CPULimitedHost,
				  #link=TCLink,
				  #autoStaticArp=True,
				  build=False,
				  ipBase=IP_BASE)	
				  	
	net.addController(name='c0',
				   controller=RemoteController,
				   ip='127.0.0.1',
				   protocol='tcp',
				   port=6633)					 
	net.start()
	dhcp = net.get('dhcp')
	startDHCPserver( dhcp, dns='8.8.8.8' )
	
	# get all hosts
	hosts = []
	for i in range(MAX_HOSTS):
		hosts.append(net.get('h%s' % (i+1)))
	
	# request ips
	for host in hosts:
		mountPrivateResolvconf( host )
		startDHCPclient( host )
		waitForIP( host )
		print host.cmd('ifconfig')
	
	attacker = net.get('attacker')
	honeyd = net.get('honeyd')
	
	path = '{pdir}/{edir}/{folder}'.format(pdir=DIR, edir=E_DIR, folder=time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime(time.time())))
	attacker.cmd('mkdir -m 777 %s' % (path))
	honeyd.cmd('install -b -m 777 /dev/null {path}/{plog}'.format(path=path, plog=P_LOG)) # workaround (honeyd demoted to run as nobody user)
	honeyd.cmd('install -b -m 777 /dev/null {path}/{slog}'.format(path=path, slog=S_LOG)) # service logging

	lines = [] # output to file
	actives = [] # active http servers
	
	lines.append("Setup with Honeyd, %s hosts, %s active" %( MAX_HOSTS, MAX_ACTIVES ) )
	
	# start http server in MAX_ACTIVES random hosts
	indices = random.sample(range(MAX_HOSTS), MAX_ACTIVES)
	for i in indices:
		hosts[i].cmd('python -m SimpleHTTPServer %s &' % ( PORT ))
		
	# start wireshark in attacker host
	print 'Starting wireshark in attacker... Waiting 15s...'
	attacker.cmd('wireshark &')
	sleep(15)

	# start wireshark in honeyd host
	print 'Starting wireshark in honeyd... Waiting 15s...'
	honeyd.cmd('wireshark &')
	sleep(15)
	
	# start honeyd daemon
	print 'Starting honeyd...'
	honeyd.cmd('sudo honeyd -l {path}/{plog} -s {path}/{slog} -p {pdir}/{hdir}/{prints} -f {pdir}/{hdir}/{config}'
				.format(path=path, pdir=DIR, hdir=H_DIR, plog=P_LOG, slog=S_LOG, prints=PRINTS, config=CONFIG))
	
	# nmap for running http servers
	print 'Starting nmap...'
	start = time.time() # start of attack
	lines.append('nmap started at %s' % ( time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start)) ))
	results = attacker.cmd('nmap -p {port} {network}'.format(port=PORT, network=IP_BASE)).split("\r\n\r\n")
	lines.append('After %s secs nmap done.' % (time.time() - start))
	
	ip_pat = re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
	http_pat = re.compile("%s/tcp\s+open\s+http" % ( PORT ), re.I)

	for result in results:
		search_obj = re.search(ip_pat, result)
		if search_obj:
			ip = search_obj.group()
			if re.search(http_pat, result):
				actives.append(ip)
				lines.append('%s active' % ( ip ))	
	lines.append('\n')
	
	# dos attack using slowhttptest slowloris mode	
	print 'Starting slowhttptest...'
	for active in actives:
		cmd = ('slowhttptest -c 1000 -H -g -o {path}/{fname} -i 10 -r 200 -t GET -u http://{url}:{port} -x 24 -p {tolerance}'
				.format(path=path, fname=active, url=active, port=PORT, tolerance=TOLERANCE))
		for out in execute( attacker, shlex.split(cmd) ):
			pass
		lines.append('After {0} secs {1} down'.format(time.time() - start, active))

	end = time.time()
	lines.append('\nTotal time: %s' % (end - start))
	out = open('%s/stats' % (os.path.expanduser(path)), 'w')
	out.write('\n'.join(lines))
	out.close()

	
	# cleanup
	stopDHCPserver( dhcp )
	for i in range(MAX_HOSTS):
		host = net.get('h%s' % (i+1))
		stopDHCPclient( host )
		unmountPrivateResolvconf( host )
	CLI( net )
	

def checkRequired():
	"Check for required executables"
	required = [ 'udhcpd', 'udhcpc', 'dnsmasq', 'curl', 'firefox' ]
	for r in required:
		if not quietRun( 'which ' + r ):
			print '* Installing', r
			print quietRun( 'apt-get install -y ' + r )
			if r == 'dnsmasq':
				# Don't run dnsmasq by default!
				print quietRun( 'update-rc.d dnsmasq disable' )


# DHCP server functions and data
DNSTemplate = """
start		10.0.0.128
end		10.0.0.250
option	subnet	255.255.255.0
option	domain	local
option	lease	86400  # seconds
"""
# option dns 8.8.8.8

def makeDHCPconfig( filename, intf, dns ):
    "Create a DHCP configuration file"
    config = (
        'interface %s' % intf,
        DNSTemplate,
        'option dns %s' % dns,
        '' )
    with open( filename, 'w' ) as f:
        f.write( '\n'.join( config ) )

def startDHCPserver( host, dns ):
    "Start DHCP server on host with specified DNS server"
    info( '* Starting DHCP server on', host, 'at', host.IP(), '\n' )
    dhcpConfig = '/tmp/%s-udhcpd.conf' % host
    makeDHCPconfig( dhcpConfig, host.defaultIntf(), dns )
    host.cmd( 'udhcpd -f', dhcpConfig,
              '1>/tmp/%s-dhcp.log 2>&1  &' % host )

def stopDHCPserver( host ):
    "Stop DHCP server on host"
    info( '* Stopping DHCP server on', host, 'at', host.IP(), '\n' )
    host.cmd( 'kill %udhcpd' )

def startDHCPclient( host ):
    "Start DHCP client on host"
    intf = host.defaultIntf()
    host.cmd( 'dhclient -v -d -r', intf )
    host.cmd( 'dhclient -v -d 1> /tmp/dhclient.log 2>&1', intf, '&' )

def stopDHCPclient( host ):
    host.cmd( 'kill %dhclient' )

def waitForIP( host ):
    "Wait for an IP address"
    info( '*', host, 'waiting for IP address' )
    while True:
        host.defaultIntf().updateIP()
        if host.IP():
            break
        info( '.' )
        sleep( 1 )
    info( '\n' )
    info( '*', host, 'is now using',
          host.cmd( 'grep nameserver /etc/resolv.conf' ) )

def mountPrivateResolvconf( host ):
    "Create/mount private /etc/resolv.conf for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'mkdir -p', etc )
    host.cmd( 'mount --bind /etc', etc )
    host.cmd( 'mount -n -t tmpfs tmpfs /etc' )
    host.cmd( 'ln -s %s/* /etc/' % etc )
    host.cmd( 'rm /etc/resolv.conf' )
    host.cmd( 'cp %s/resolv.conf /etc/' % etc )

def unmountPrivateResolvconf( host ):
    "Unmount private /etc dir for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'umount /etc' )
    host.cmd( 'umount', etc )
    host.cmd( 'rmdir', etc )
    
if __name__ == '__main__':
	setLogLevel('info')
	dosTest()
