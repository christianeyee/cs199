#!/usr/bin/python

'''

Mininet setup for testing

'''

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost, RemoteController
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.util import pmonitor

from signal import SIGINT
from subprocess import PIPE
from time import sleep

import shlex
import time
import re
import sys
import random

IP_DHCP = '10.0.0.251'
IP_ATTACKER = '10.0.0.252'
IP_HONEYD = '10.0.0.253'
MAX_HOSTS = 5

class SingleSwitchTopo(Topo):
	"Single switch connected to n hosts."
	def __init__(self, **opts):
		Topo.__init__(self, **opts)
	
		switch = self.addSwitch('s1')
		
		# set up dhcp server
		dhcp = self.addHost( 'dhcp', ip=IP_DHCP )
		self.addLink(dhcp, switch) 

		# set up hosts
		for i in range(MAX_HOSTS):
			host = self.addHost('h%s' % (i+1))	
			self.addLink(host, switch)

		# set up attacker		 
		attacker = self.addHost('attacker', ip=IP_ATTACKER)
		self.addLink(attacker, switch)
		
		# set up honeyd
		honeyd = self.addHost('honeyd', ip=IP_HONEYD)
		self.addLink(honeyd, switch)
		

def test():
	topo = SingleSwitchTopo()
	net = Mininet(topo=topo,
				  cleanup = True,
				  #host=CPULimitedHost,
				  #link=TCLink,
				  #autoStaticArp=True,
				  build=False,
				  ipBase='10.0.0.0/24')	
				  	
	net.addController(name='c0',
				   controller=RemoteController,
				   ip='127.0.0.1',
				   protocol='tcp',
				   port=6633)					 
	net.start()
	
	#rootnode = connectToInternet( net, 's1' ) # changed this to use /24
	dhcp = net.get('dhcp')
	startDHCPserver( net.get('dhcp'), dns='8.8.8.8' )
	
	# get all hosts
	hosts = []
	for i in range(MAX_HOSTS):
		hosts.append(net.get('h%s' % (i+1)))
	
	# request ips
	for host in hosts:
		mountPrivateResolvconf( host )
		startDHCPclient( host )
		waitForIP( host )
		print host.cmd('ifconfig')

	
	# cleanup
	stopDHCPserver( dhcp )
	for i in range(MAX_HOSTS):
		host = net.get('h%s' % (i+1))
		stopDHCPclient( host )
		unmountPrivateResolvconf( host )
	CLI( net )
	

def checkRequired():
	"Check for required executables"
	required = [ 'udhcpd', 'udhcpc', 'dnsmasq', 'curl', 'firefox' ]
	for r in required:
		if not quietRun( 'which ' + r ):
			print '* Installing', r
			print quietRun( 'apt-get install -y ' + r )
			if r == 'dnsmasq':
				# Don't run dnsmasq by default!
				print quietRun( 'update-rc.d dnsmasq disable' )


# DHCP server functions and data
DNSTemplate = """
start		10.0.0.128
end		10.0.0.250
option	subnet	255.255.255.0
option	domain	local
option	lease	86400  # seconds
"""
# option dns 8.8.8.8

def makeDHCPconfig( filename, intf, dns ):
    "Create a DHCP configuration file"
    config = (
        'interface %s' % intf,
        DNSTemplate,
        #'option router %s' % gw,
        'option dns %s' % dns,
        '' )
    with open( filename, 'w' ) as f:
        f.write( '\n'.join( config ) )

def startDHCPserver( host, dns ):
    "Start DHCP server on host with specified DNS server"
    info( '* Starting DHCP server on', host, 'at', host.IP(), '\n' )
    dhcpConfig = '/tmp/%s-udhcpd.conf' % host
    makeDHCPconfig( dhcpConfig, host.defaultIntf(), dns )
    host.cmd( 'udhcpd -f', dhcpConfig,
              '1>/tmp/%s-dhcp.log 2>&1  &' % host )

def stopDHCPserver( host ):
    "Stop DHCP server on host"
    info( '* Stopping DHCP server on', host, 'at', host.IP(), '\n' )
    host.cmd( 'kill %udhcpd' )

def startDHCPclient( host ):
    "Start DHCP client on host"
    intf = host.defaultIntf()
    host.cmd( 'dhclient -v -d -r', intf )
    host.cmd( 'dhclient -v -d 1> /tmp/dhclient.log 2>&1', intf, '&' )

def stopDHCPclient( host ):
    host.cmd( 'kill %dhclient' )

def waitForIP( host ):
    "Wait for an IP address"
    info( '*', host, 'waiting for IP address' )
    while True:
        host.defaultIntf().updateIP()
        if host.IP():
            break
        info( '.' )
        sleep( 1 )
    info( '\n' )
    info( '*', host, 'is now using',
          host.cmd( 'grep nameserver /etc/resolv.conf' ) )

def mountPrivateResolvconf( host ):
    "Create/mount private /etc/resolv.conf for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'mkdir -p', etc )
    host.cmd( 'mount --bind /etc', etc )
    host.cmd( 'mount -n -t tmpfs tmpfs /etc' )
    host.cmd( 'ln -s %s/* /etc/' % etc )
    host.cmd( 'rm /etc/resolv.conf' )
    host.cmd( 'cp %s/resolv.conf /etc/' % etc )

def unmountPrivateResolvconf( host ):
    "Unmount private /etc dir for host"
    etc = '/tmp/etc-%s' % host
    host.cmd( 'umount /etc' )
    host.cmd( 'umount', etc )
    host.cmd( 'rmdir', etc )
    
if __name__ == '__main__':
	setLogLevel('info')
	test()
