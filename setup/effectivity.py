#!/usr/bin/python

'''

Experiment for testing attack time.

'''


from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.log import setLogLevel, info
from mininet.cli import CLI

from signal import SIGINT
from subprocess import PIPE
from time import sleep

import os
import re
import shlex
import sys
import time
import random

from constants import *


def execute(attacker, command):

	po = attacker.popen(command, stdout=PIPE, stderr=PIPE, shell=True, bufsize=1)

	count = 0
	keywords = ['service', 'available', 'NO']
	
	for line in iter(po.stdout.readline, b""):
		# terminate after 3rd service down result
		if count >= TOLERANCE:
			po.send_signal( SIGINT )
			return
			
		# look for service down result
		if all(word in line for word in keywords):
			count += 1
			
		yield line


class SingleSwitchTopo(Topo):
	"Single switch connected to n hosts."
	def __init__(self, **opts):
		Topo.__init__(self, **opts)
	
		switch = self.addSwitch('s1')
		
		first = FROM + MAX_HOSTS
		
		# set up hosts
		for i in range(MAX_HOSTS):
			host = self.addHost('h%s' % (i+1), ip='%s%s' % ( IP_PREFIX, first + i ))	
			self.addLink(host, switch)

		# set up attacker		 
		attacker = self.addHost('attacker', ip=IP_ATTACKER)
		self.addLink(attacker, switch)
		
		# set up honeyd
		honeyd = self.addHost('honeyd', ip=IP_HONEYD)
		self.addLink(honeyd, switch)
		

def dosTest(percent, expath):
	topo = SingleSwitchTopo()
	net = Mininet(topo=topo,
				  cleanup = True,
				  build=False,
				  ipBase=IP_BASE)	
				  	
	net.addController(name='c0',
				   controller=RemoteController,
				   ip='127.0.0.1',
				   protocol='tcp',
				   port=6633)					 
	net.start()
	attacker = net.get('attacker')
	honeyd = net.get('honeyd')
	
	
	# for parsing nmap output
	ip_pat = re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
	http_pat = re.compile("%s/tcp\s+open\s+http" % ( PORT ), re.I)
	

	# prepare output files and directory
	attacker.cmd('mkdir -p -m 777 %s' % (expath))
	output = open('{fp}/results'.format(fp=os.path.expanduser(expath)), 'a')
	
	
	# total number of honeypot web servers
	total_fakes = percent * MAX_HOSTS / 100
	
	
	print 'Starting Honeyd with %s web servers' % ( total_fakes )
	honeyd.cmd('sudo honeyd -p {pdir}/{hdir}/{prints} -f {pdir}/{hdir}/{config}{p}'
				.format(fp=expath, pdir=DIR, hdir=H_DIR, prints=PRINTS, config=CONFIG, p=percent))
	
	
	# start web servers
	legit_ips = [] # list of legitimate host ips
	for i in range(MAX_HOSTS): # next INCREMENT hosts
		host = net.get('h%s' % (i+1))

		# run http server
		print "Starting Python SimpleHTTPServer at %s" % host
		host.cmd('pushd {pdir}/{mdir}'.format(pdir=DIR, mdir=M_DIR))
		host.cmd('python -m SimpleHTTPServer %s &' % ( PORT ))
		host.cmd('popd')
		
		legit_ips.append(str(host.IP()))

	
	print 'Starting attack with %s Honeyd web servers...' % ( total_fakes )
	a_output = open('{fp}/results_{p}'.format(fp=os.path.expanduser(expath), p=percent), 'w')	
	lines = [] # output to file
	actives = [] # active http servers
	
	lines.append("%s active legitimate web servers" % ( MAX_HOSTS ) )
	
	# nmap for running http servers
	print 'Starting NMAP...'
	start = time.time() # start of attack
	lines.append('nmap started at %s' % ( time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start)) ))
	nmap = attacker.cmd('nmap -p {port} {network}'.format(port=PORT, network=IP_BASE)).split('\r\n\r\n')
	lines.append('After %s secs nmap done.' % (time.time() - start))

	for result in nmap:
		search_obj = re.search(ip_pat, result)
		if search_obj:
			ip = search_obj.group()
			if re.search(http_pat, result):
				actives.append(ip)
				if str(ip) in legit_ips: legit_ips.remove(str(ip))
				lines.append('%s active' % ( ip ))
				

	# dos attack using slowhttptest slowloris mode	
	print 'Starting SlowHTTPTest...'
	for active in actives:
		cmd = ('slowhttptest -c 1000 -H -i 10 -r 200 -t GET -u http://{url}:{port} -x 24 -p {tolerance}'
				.format(url=active, port=PORT, tolerance=TOLERANCE))
		for out in execute( attacker, shlex.split(cmd) ):
			pass
		lines.append('After {t} secs {host} down'.format(t=time.time()-start, host=active))

	total = ('Total attack time with {pots} Honeyd web servers: {runtime}'
				.format(pots=total_fakes, runtime=time.time() - start))
	
	lines.append('Total apparent active hosts: %s' % (len(actives)))
	lines.append('Total legitimate hosts not found: %s' % (len(legit_ips)))
	lines.append('Total time: %s' % (total))
	a_output.write('\n'.join(lines))
	a_output.close()
		
	output.write('%s\n' % ( total ))
	output.close()	
	
	print 'Ending test with %s Honeyd web servers...' % ( total_fakes )
	# kill running honeyd daemon
	honeyd.cmd('sudo pkill -SIGINT honeyd')
	net.stop()

    
if __name__ == '__main__':
	setLogLevel('info')
	dosTest(100, 
			'{pdir}/{edir}/{folder}'
				.format(pdir=DIR, edir=E_DIR, folder=time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime(time.time()))))
