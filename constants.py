# network address range
FROM = 1
TO = 100
IP_PREFIX = '10.0.0.'

MAX_HOSTS = (TO - FROM + 1) / 2 # max connecting hosts / max honeypots
INCREMENT = 1 # num hosts connecting at a time

PERCENTAGES = [0, 25, 50, 75, 100] # % 'unused' ips taken by honeyd


CONFIG = 'conf_' # generated config files prefix
OS = ['mac', 'windows'] # honeyd template names
DEFAULT = """
create default
set default personality "Linux 3.9"
set default default udp action reset
set default default icmp action open
set default default tcp action reset
"""
MAC = """
create mac
set mac personality "Apple Mac OS X 10.8.2 (Mountain Lion) (Darwin 12.2.0)"
set mac default udp action reset
set mac default icmp action open
set mac default tcp action open
add mac tcp port 80 "sh scripts/win32/win2k/iis.sh $ipsrc $sport $ipdst $dport"
"""
WINDOWS = """
create windows
set windows personality "Microsoft Windows 7 Ultimate"
set windows default udp action reset
set windows default icmp action open
set windows default tcp action open
add windows tcp port 80 "sh scripts/win32/win2k/iis.sh $ipsrc $sport $ipdst $dport"
"""



DIR = '~/pox'
E_DIR = 'experiments'
H_DIR = 'honeyd'
M_DIR = 'setup'
PRINTS = 'nmap-os-db'
P_LOG = 'honeyd.packet.log'
S_LOG = 'honeyd.service.log'

HONEYD_SERVER_RESPONSE = 'Served from virtual honeypot' # as configured in iis.sh script
LEGIT_SERVER_RESPONSE = 'Served from legitimate host' # as configured in mininet/index.html

IP_BASE = '10.0.0.0/24'
IP_DHCP = '10.0.0.251'
IP_ATTACKER = '10.0.0.252'
IP_HONEYD = '10.0.0.253'

PORT = 80 # port where http server is running
TOLERANCE = 3 # num failed requests before considered down
RESILIENCE = 5 # num tries before wget gives up
PATIENCE = 10 # max time before wget retry
GRACE = 10 # grace period to clear flows

