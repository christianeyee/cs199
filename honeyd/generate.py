'''

Script to generate honeyd configuration files.
Output files named conf_{m} - m is the % of ips binded

'''
import os

from constants import *


def create(filename, percentages=None):
	ips = [] # list of claimable ips in the network
	
	for i in range(FROM, TO+1):
		ips.append('{prefix}{n}'.format(prefix=IP_PREFIX, n=i))
		
	
	# generate config file for connectivity
	if percentages is None:
		total = TO - FROM + 1 # total number of ips
		binds = []
		for i in range(total):
			binds.append('bind {ip} {temp}'.format(ip=ips[i], temp=OS[i%2]))
			
			fp = '%s%s' % (filename, 'all')
			conf = open(fp, 'w')
			conf.write('%s' % (DEFAULT))
			conf.write('%s' % (MAC))
			conf.write('%s' % (WINDOWS))
			conf.write('\n')
			conf.write('\n'.join(binds))
			
			conf.close()
			os.chmod(fp, 0o777)
	
	
	# generate config files for brienne
	else:
		for m in percentages:
			maximum = int(m*MAX_HOSTS/100) # m% of total free ips
			binds = []
			for i in range(maximum):
				binds.append('bind {ip} {temp}'.format(ip=ips[i], temp=OS[i%2]))
			
			fp = '%s%s' % (filename, m)
			conf = open(fp, 'w')
			conf.write('%s' % (DEFAULT))
			conf.write('%s' % (MAC))
			conf.write('%s' % (WINDOWS))
			conf.write('\n')
			conf.write('\n'.join(binds))
			
			conf.close()
			os.chmod(fp, 0o777)
		

if __name__ == '__main__':
	create(CONFIG, sys.argv)





