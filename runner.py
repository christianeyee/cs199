'''

Script for running all experiments.
Run with sudo.

'''

from honeyd import generate
from setup import connectivity
from setup import effectivity

import subprocess
import signal
import shlex
import sys
import os
import time

from constants import *


# create output folder
name = time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime(time.time()))
expath = '{pdir}/{edir}/{folder}'.format(pdir=DIR, edir=E_DIR, folder=name)



# generate honeyd config file
print '* Generating Honeyd config file...'
generate.create('honeyd/%s' % (CONFIG))

print 'Tesing host connectivity...'
# Clean up mininet
cleanup = subprocess.Popen(shlex.split('sudo mn -c'))
cleanup.wait()

# Run POX controller
pid = subprocess.Popen(shlex.split('./pox.py l3_learning')).pid
time.sleep(3)

# Run test
connectivity.netBuild(expath + '/connectivity')

# Stop POX controller
os.kill(pid, signal.SIGINT)



'''
# generate honeyd config files
print '* Generating Honeyd config files...'
generate.create('honeyd/%s' % (CONFIG), PERCENTAGES)

# run all experiments
for p in PERCENTAGES:
	print '* Starting new experiment in 3 secs...'
	time.sleep(3)
	
	# Clean up mininet
	cleanup = subprocess.Popen(shlex.split('sudo mn -c'))
	cleanup.wait()
	
	# Run POX controller
	pid = subprocess.Popen(shlex.split('./pox.py l3_learning_2')).pid
	time.sleep(3)
	
	# Run test
	effectivity.dosTest(p, expath)
	
	# Stop POX controller
	os.kill(pid, signal.SIGINT)
'''


sys.exit()
